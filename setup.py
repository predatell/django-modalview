import os
from setuptools import setup, find_packages


def README():
    return open('README.md').read()

setup(
    name='django-modalview',
    version='0.1.6',
    packages=find_packages(),
    include_package_data=True,
    license='Apache License 2.0',
    description='Django app to add generic views.',
    long_description=README(),
    url='https://gitlab.com/predatell/django-modalview',
    author='Valentin Monte',
    author_email='valentin.monte@optiflows.com',
    install_requires=[
        'django>=1.4',
    ],
    setup_requires=[
        'setuptools_git>=1.0',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
